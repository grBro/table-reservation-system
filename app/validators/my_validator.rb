class MyValidator < ActiveModel::Validator
  def validate record
    return if record.start_time.nil? or record.end_time.nil?
    validate_time_before_present record
    validate_time_wrong_direction record
    validate_time_equality record
    @reservation = record.class.where(table_id: record.table.id)
    @reservation = @reservation.where.not(id: record.id) if record.id.present?
    validate_time_overlapping record
  end

  def validate_time_before_present record
    record.errors.add(:start_time, 'in the past') if record.start_time < Time.now
    record.errors.add(:end_time, 'in the past') if record.end_time < Time.now
  end

  def validate_time_wrong_direction record
    record.errors.add(:start_time, 'greater than end_time') if record.start_time > record.end_time
    record.errors.add(:end_time, 'less than start_time') if record.end_time < record.start_time
  end

  def validate_time_equality record
    record.errors.add(:start_time, 'is equal to end time') if record.start_time == record.end_time
  end

  def validate_time_overlapping record
    overlapping = @reservation.where('(start_time > :start_time AND end_time < :end_time)
                                   OR (start_time <= :start_time AND end_time > :start_time)
                                   OR (start_time < :end_time AND end_time >= :end_time)',
                                   start_time: record.start_time, end_time: record.end_time)
    record.errors.add(:start_time, 'wrong period') if overlapping.any?
  end
end