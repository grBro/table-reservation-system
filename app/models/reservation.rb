class Reservation < ActiveRecord::Base
  belongs_to :table
  include ActiveModel::Validations
  validates_with MyValidator
  validates :start_time, :end_time, :table_id, presence: true
end
