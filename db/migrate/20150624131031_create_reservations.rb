class CreateReservations < ActiveRecord::Migration
  def change
    create_table :tables do |t|
      t.string :number

      t.timestamps null: false
    end
    
    create_table :reservations do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.references :table, index: true

      t.timestamps null: false
    end
    add_foreign_key :reservations, :tables
  end
end
