require 'rails_helper'

RSpec.describe Reservation, :type => :model do
  it { should validate_presence_of :start_time }
  it { should validate_presence_of :end_time }
  it { should validate_presence_of :table_id }

  let(:table) { create :table }
  let(:table2) { create :table }

  it 'should add reservation with valid start_time and end_time' do
    record = described_class.new(start_time: 1.hour.from_now, end_time: 2.hours.from_now, table_id: table.id)
    expect(record).to be_valid
  end

  it 'should disallow to create reservation with old date' do
    record = described_class.new(start_time: 2.hours.ago, end_time: 3.hours.ago, table_id: table.id)
    expect(record).to_not be_valid
  end

  it 'should disallow to create reservation if end time less than start time' do
    record = described_class.new(start_time: 2.hours.from_now, end_time: 1.hour.from_now, table_id: table.id)
    expect(record).to_not be_valid
  end

  it 'should disallow to create reservation if start time is equal to end time' do
    time = 2.hours.from_now
    record = described_class.new(start_time: time, end_time: time, table_id: table.id)
    expect(record).to_not be_valid
  end

  context '#create' do
    it 'should allow to create reservation with no overlapping' do
      start_time = 3.hours.from_now
      end_time = 6.hours.from_now
      record1 = described_class.create(start_time: start_time, end_time: end_time, table_id: table.id)
      record2 = described_class.create(start_time: 1.hour.from_now, end_time: 2.hours.from_now, table_id: table.id)
      record3 = described_class.create(start_time: 2.hours.from_now, end_time: start_time, table_id: table.id)
      record4 = described_class.create(start_time: end_time, end_time: 7.hours.from_now, table_id: table.id)
      record5 = described_class.create(start_time: 7.hours.from_now, end_time: 8.hours.from_now, table_id: table.id)
      expect(record1).to be_valid
      expect(record2).to be_valid
      expect(record3).to be_valid
      expect(record4).to be_valid
      expect(record5).to be_valid
    end

    it 'should allow to create reservation with overlapping on different tables' do
      record1 = described_class.create(start_time: 3.hours.from_now, end_time: 6.hours.from_now, table_id: table.id)
      record2 = described_class.create(start_time: 3.hours.from_now, end_time: 7.hours.from_now, table_id: table2.id)
      expect(record1).to be_valid
      expect(record2).to be_valid
    end

    it 'should disallow to create reservation with the same time overlapping' do
      start_time = 3.hours.from_now
      end_time = 6.hours.from_now
      record1 = described_class.create(start_time: start_time, end_time: end_time, table_id: table.id)
      record2 = described_class.create(start_time: start_time, end_time: end_time, table_id: table.id)
      expect(record1).to be_valid
      expect(record2).to_not be_valid
    end

    it 'should disallow to create reservation with start time overlapping' do
      record1 = described_class.create(start_time: 3.hours.from_now, end_time: 6.hours.from_now, table_id: table.id)
      record2 = described_class.create(start_time: 4.hours.from_now, end_time: 7.hours.from_now, table_id: table.id)
      expect(record1).to be_valid
      expect(record2).to_not be_valid
    end

    it 'should disallow to create reservation with end time overlapping' do
      record1 = described_class.create(start_time: 3.hours.from_now, end_time: 6.hours.from_now, table_id: table.id)
      record2 = described_class.create(start_time: 2.hours.from_now, end_time: 4.hours.from_now, table_id: table.id)
      expect(record1).to be_valid
      expect(record2).to_not be_valid
    end

    it 'should disallow to create reservation with surrounded overlapping' do
      record1 = described_class.create(start_time: 3.hours.from_now, end_time: 6.hours.from_now, table_id: table.id)
      record2 = described_class.create(start_time: 2.hours.from_now, end_time: 7.hours.from_now, table_id: table.id)
      expect(record1).to be_valid
      expect(record2).to_not be_valid
    end

    it 'should disallow to create reservation with inner overlapping' do
      record1 = described_class.create(start_time: 3.hours.from_now, end_time: 6.hours.from_now, table_id: table.id)
      record2 = described_class.create(start_time: 4.hour.from_now, end_time: 5.hours.from_now, table_id: table.id)
      expect(record1).to be_valid
      expect(record2).to_not be_valid
    end
  end

  context '#update' do
    it 'should allow update reservations without overlapping' do
      record1 = described_class.create(start_time: 3.hours.from_now, end_time: 6.hours.from_now, table_id: table.id)
      record2 = described_class.create(start_time: 7.hours.from_now, end_time: 9.hours.from_now, table_id: table.id)
      expect(record1).to be_valid
      expect(record2).to be_valid
      record2.start_time = 8.hours.from_now
      expect(record2).to be_valid
    end

    it 'should disallow update reservations with start time overlapping' do
      record1 = described_class.create(start_time: 3.hours.from_now, end_time: 6.hours.from_now, table_id: table.id)
      record2 = described_class.create(start_time: 7.hours.from_now, end_time: 9.hours.from_now, table_id: table.id)
      expect(record1).to be_valid
      expect(record2).to be_valid
      record2.start_time = 5.hours.from_now
      expect(record2).to_not be_valid
    end

    it 'should disallow update reservations with end time overlapping' do
      record1 = described_class.create(start_time: 3.hours.from_now, end_time: 6.hours.from_now, table_id: table.id)
      record2 = described_class.create(start_time: 1.hours.from_now, end_time: 2.hours.from_now, table_id: table.id)
      expect(record1).to be_valid
      expect(record2).to be_valid
      record2.end_time = 5.hours.from_now
      expect(record2).to_not be_valid
    end

    it 'should disallow to update reservation with surrounded overlapping' do
      record1 = described_class.create(start_time: 3.hours.from_now, end_time: 6.hours.from_now, table_id: table.id)
      record2 = described_class.create(start_time: 7.hours.from_now, end_time: 8.hours.from_now, table_id: table.id)
      expect(record1).to be_valid
      expect(record2).to be_valid
      record2.start_time = 2.hours.from_now
      expect(record2).to_not be_valid
    end

    it 'should disallow to update reservation with inner overlapping' do
      record1 = described_class.create(start_time: 3.hours.from_now, end_time: 6.hours.from_now, table_id: table.id)
      record2 = described_class.create(start_time: 7.hour.from_now, end_time: 8.hours.from_now, table_id: table.id)
      expect(record1).to be_valid
      expect(record2).to be_valid
      record2.start_time = 4.hours.from_now
      record2.end_time = 5.hours.from_now
      expect(record2).to_not be_valid
    end
  end
end
