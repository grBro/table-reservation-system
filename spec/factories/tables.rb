FactoryGirl.define do
  sequence(:number) { |n| "#{n}" }

  factory :table do
    number
  end
end
