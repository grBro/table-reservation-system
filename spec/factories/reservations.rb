FactoryGirl.define do
  factory :reservation do
    association :table, factory: :table
  end
end
